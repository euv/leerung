# Offene Punkte
- [ ] Design der Anwendung
- [ ] Textelemente in der Anwendung (fachlich)
- [ ] Lizenzen (Closed / Open)
- [ ] Fachliche Pflege (Planungstage)
- [ ] Umgang mit Schnittstellen
- [ ] Dokumentation auf Webseite
