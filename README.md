# Leerungsabfrage 2017
Im Rahmen der Entwicklung steht eine Version der Leerungsabfrage zu Verfügung.

Die Version ist aus dem Internet erreichbar, behält sich aber vor das der Dienst
nicht verfügbar ist oder nicht fehlerfrei funktioniert.

Bei Anregungen bitte eine Mail an: presse@euv-stadtbetrieb.de

Bei Programmfehlern bitte an: administration@euv-stadtbetrieb.de

## URL
Die Entwicklungsseite finden man unter:
- https://leerung-dev.euv-stadtbetrieb.de

Die Produktion unter:
- https://leerung.euv-stadtbetrieb.de

# API
Die Leerungsabfrage nutzt zwei REST-APIs. Diese sind:

- Abfrage der Leerungen: https://api.euv-stadtbetrieb.de/leerung/v1/swagger-ui.html
- Abfrage der Planungstage: https://api.euv-stadtbetrieb.de/planungstage/swagger-ui.html

# Container
Der Container ist soweit vorbereitet das er mit Templates konfiguriert werden kann.
Hier für sind die Verzeichnisse `/euv/config/WEB-INF` und `/euv/config/tomcat` vorhanden.

Die Applikation hat die Bindung für Spring-Boot an Redis für das Sessionmanagement inkludiert.

## WEB-INF
Alle Dateien in diesen Verzeichnis werden mit `dockerize` als Template behandelt und in
das `WEB-INF` Verzeichnis des Applikation, vor dem Start, kopiert.

## tomcat
In diesem Verzeichnis werden alle Dateien mit `dockerize`als Template behandelt und in das
`conf` Verzeichnis von Tomcat kopiert.
