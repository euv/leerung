/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.OptionGroupModel;
import org.apache.tapestry5.OptionModel;
import org.apache.tapestry5.internal.OptionModelImpl;
import org.apache.tapestry5.util.AbstractSelectModel;
import org.euv.leerung.rest.model.Strasse;
import org.springframework.stereotype.Component;

/**
 * A select model for selecting a strasse from the API. 
 * @author Marcus Schneider
 */
@Component
public class StrassenIdSelectModel extends AbstractSelectModel {
	
	private List<Strasse> strassen;
	
	public StrassenIdSelectModel(List<Strasse> strassen) {
		this.strassen = strassen;
	}

	@Override
	public List<OptionGroupModel> getOptionGroups() {
		return null;
	}

	@Override
	public List<OptionModel> getOptions() {
		List<OptionModel> options = new ArrayList<>();
		for(Strasse strasse :strassen) {
			options.add(new OptionModelImpl(strasse.getName(), strasse.getId()));
		}
		return options;
	}

}
