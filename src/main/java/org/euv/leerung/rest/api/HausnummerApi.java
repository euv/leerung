/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.api;

import java.util.List;
import javax.ws.rs.*;

import org.euv.leerung.rest.model.ErrorEntity;
import org.euv.leerung.rest.model.Hausnummer;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

/**
 * Leerungen REST API
 *
 * <p>REST API für die Abfrage von Leerungen
 *
 */
@Path("/")
@Api(value = "/")
public interface HausnummerApi  {

    /**
     * Gibt die Infos zu einer Hausnummer zur�ck
     *
     */
    @GET
    @Path("/haus/get/{haId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Gibt die Infos zu einer Hausnummer zur�ck", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Hausnummer.class),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Die Hausnummer wurde nicht gefunden.", response = ErrorEntity.class),
        @ApiResponse(code = 500, message = "Ein Fehler mit der Datenbank ist aufgetretten.") })
    public Hausnummer getByIdUsingGET(@PathParam("haId") Long haId, @QueryParam("strasse")@DefaultValue("false") Boolean strasse);

    /**
     * Liste alle Hausnummern auf der Straße mit der stId auf.
     *
     */
    @GET
    @Path("/haus/list/{stId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste alle Hausnummern auf der Straße mit der stId auf.", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Hausnummer.class, responseContainer = "array"),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Auf der Straße wurden keine Hausnummern gefunden.", response = ErrorEntity.class),
        @ApiResponse(code = 500, message = "Ein Fehler mit der Datenbank ist aufgetretten.") })
    public List<Hausnummer> listUsingGET(@PathParam("stId") Long stId, @QueryParam("strasse")@DefaultValue("false") Boolean strasse);
}

