/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.api;

import javax.ws.rs.*;

import org.euv.leerung.rest.model.ErrorEntity;
import org.euv.leerung.rest.model.Leerungen;
import org.euv.leerung.rest.model.LeerungenFeiertagverschiebung;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

/**
 * Leerungen REST API
 *
 * <p>REST API für die Abfrage von Leerungen
 *
 */
@Path("/")
@Api(value = "/")
public interface LeerungApi  {

    /**
     * Liste der Leerungen für die Hausnummer
     *
     */
    @GET
    @Path("/leerung/get/{haId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste der Leerungen für die Hausnummer", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Leerungen.class),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Für diese Hausnummer wurden keine Leerungen gefunden.", response = ErrorEntity.class),
        @ApiResponse(code = 500, message = "Ein Fehler mit der Datenbank ist aufgetretten.") })
    public Leerungen getLeerungByHaIdUsingGET(@PathParam("haId") Long haId, @QueryParam("full")@DefaultValue("false") Boolean full);

    /**
     * Liste der Leerungen für die Hausnummer mit Feiertagsverschiebung
     *
     */
    @GET
    @Path("/leerung/get/feiertagsverschiebung/{haId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste der Leerungen für die Hausnummer mit Feiertagsverschiebung", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = LeerungenFeiertagverschiebung.class),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Für diese Hausnummer wurden keine Leerungen gefunden.", response = ErrorEntity.class),
        @ApiResponse(code = 500, message = "Ein Fehler mit der Datenbank ist aufgetretten.") })
    public LeerungenFeiertagverschiebung getLeerungFeiertagsverschiebungByHaIdUsingGET(@PathParam("haId") Long haId, @QueryParam("full")@DefaultValue("false") Boolean full);
}

