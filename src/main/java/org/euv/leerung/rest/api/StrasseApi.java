/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.api;

import java.util.List;
import javax.ws.rs.*;

import org.euv.leerung.rest.model.ErrorEntity;
import org.euv.leerung.rest.model.Strasse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

/**
 * Leerungen REST API
 *
 * <p>REST API für die Abfrage von Leerungen
 *
 */
@Path("/")
@Api(value = "/")
public interface StrasseApi  {

    /**
     * Informationen zu einer Straßen ID abrufen
     *
     */
    @GET
    @Path("/strasse/get/{stId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Informationen zu einer Straßen ID abrufen", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Strasse.class),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Eine Straße mit dieser ID ist nicht vorhanden", response = ErrorEntity.class),
        @ApiResponse(code = 500, message = "Ein Fehler mit der Datenbank ist aufgetretten") })
    public Strasse getIdUsingGET(@PathParam("stId") Long stId);

    /**
     * Listet alle Straßen mit entsprechenden Anfang auf
     *
     */
    @GET
    @Path("/strasse/list/{strasse}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Listet alle Straßen mit entsprechenden Anfang auf", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Strasse.class, responseContainer = "array"),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Es wurden keine Straßen gefunden die auf den Filter passen", response = ErrorEntity.class),
        @ApiResponse(code = 500, message = "Ein Fehler mit der Datenbank ist aufgetretten") })
    public List<Strasse> listByNameUsingGET(@PathParam("strasse") String strasse, @QueryParam("size")@DefaultValue("10") Integer size);

    /**
     * Listet alle Straßen mit bestand der Anfrage auf
     *
     */
    @GET
    @Path("/strasse/list-part/{strasse}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Listet alle Straßen mit bestand der Anfrage auf", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Strasse.class, responseContainer = "array"),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Es wurden keine Straßen gefunden die auf den Filter passen", response = ErrorEntity.class),
        @ApiResponse(code = 500, message = "Ein Fehler mit der Datenbank ist aufgetretten") })
    public List<Strasse> listByPartNameUsingGET(@PathParam("strasse") String strasse, @QueryParam("size")@DefaultValue("10") Integer size);

    /**
     * Liste aller Straßen im Stadtgebiet
     *
     */
    @GET
    @Path("/strasse/list")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste aller Straßen im Stadtgebiet", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Strasse.class, responseContainer = "array"),
        @ApiResponse(code = 400, message = "Abfrage war nicht korrekt"),
        @ApiResponse(code = 404, message = "Ein Fehler mit der Datenbank ist aufgetretten", response = ErrorEntity.class) })
    public List<Strasse> listUsingGET1();
}

