/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.model;

import io.swagger.annotations.ApiModel;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Informationen über eine Straße
 **/
@ApiModel(description="Informationen über eine Straße")
public class Strasse  {
  
  @ApiModelProperty(required = true, value = "Straßen ID")
 /**
   * Straßen ID  
  **/
  private Long id = null;

  @ApiModelProperty(required = true, value = "Straßenname")
 /**
   * Straßenname  
  **/
  private String name = null;
 /**
   * Straßen ID
   * @return id
  **/
  @JsonProperty("id")
  public Long getId() {
    return id;
  }


 /**
   * Straßenname
   * @return name
  **/
  @JsonProperty("name")
  public String getName() {
    return name;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Strasse {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

