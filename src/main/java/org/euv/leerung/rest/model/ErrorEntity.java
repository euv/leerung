/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.model;

import io.swagger.annotations.ApiModel;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Fehler Informationen
 **/
@ApiModel(description="Fehler Informationen")
public class ErrorEntity  {
  
  @ApiModelProperty(required = true, value = "Fehler Code")
 /**
   * Fehler Code  
  **/
  private Integer code = null;

  @ApiModelProperty(required = true, value = "Fehlermeldung")
 /**
   * Fehlermeldung  
  **/
  private String message = null;
 /**
   * Fehler Code
   * @return code
  **/
  @JsonProperty("code")
  public Integer getCode() {
    return code;
  }


 /**
   * Fehlermeldung
   * @return message
  **/
  @JsonProperty("message")
  public String getMessage() {
    return message;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ErrorEntity {\n");
    
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    message: ").append(toIndentedString(message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

