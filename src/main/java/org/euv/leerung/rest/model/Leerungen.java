/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.model;

import io.swagger.annotations.ApiModel;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Datumsangabe zu den Leerungen bei einer Hausnummer
 **/
@ApiModel(description="Datumsangabe zu den Leerungen bei einer Hausnummer")
public class Leerungen  {
  
  @ApiModelProperty(example = "['2016-02-12','2016-03-23']", required = true, value = "Datumsliste der Leerungen der Blauen-Tonne")
 /**
   * Datumsliste der Leerungen der Blauen-Tonne  
  **/
  private String blau = null;

  @ApiModelProperty(example = "['2016-02-12','2016-03-23']", required = true, value = "Datumsliste der Leerungen der Braunen-Tonne")
 /**
   * Datumsliste der Leerungen der Braunen-Tonne  
  **/
  private String braun = null;

  @ApiModelProperty(example = "['2016-02-12','2016-03-23']", required = true, value = "Datumsliste der Leerungen der Gelben-Tonne")
 /**
   * Datumsliste der Leerungen der Gelben-Tonne  
  **/
  private String gelb = null;

  @ApiModelProperty(example = "['2016-02-12','2016-03-23']", required = true, value = "Datumsliste der Leerungen der Grauen-Tonne")
 /**
   * Datumsliste der Leerungen der Grauen-Tonne  
  **/
  private String grau = null;

  @ApiModelProperty(value = "Die Hausnummer ID")
 /**
   * Die Hausnummer ID  
  **/
  private Long haId = null;

  @ApiModelProperty(value = "Die Hausnummer mit Zusatz")
 /**
   * Die Hausnummer mit Zusatz  
  **/
  private String nummer = null;

  @ApiModelProperty(value = "Der Straßenname")
 /**
   * Der Straßenname  
  **/
  private String strasse = null;
 /**
   * Datumsliste der Leerungen der Blauen-Tonne
   * @return blau
  **/
  @JsonProperty("blau")
  public String getBlau() {
    return blau;
  }


 /**
   * Datumsliste der Leerungen der Braunen-Tonne
   * @return braun
  **/
  @JsonProperty("braun")
  public String getBraun() {
    return braun;
  }


 /**
   * Datumsliste der Leerungen der Gelben-Tonne
   * @return gelb
  **/
  @JsonProperty("gelb")
  public String getGelb() {
    return gelb;
  }


 /**
   * Datumsliste der Leerungen der Grauen-Tonne
   * @return grau
  **/
  @JsonProperty("grau")
  public String getGrau() {
    return grau;
  }


 /**
   * Die Hausnummer ID
   * @return haId
  **/
  @JsonProperty("haId")
  public Long getHaId() {
    return haId;
  }


 /**
   * Die Hausnummer mit Zusatz
   * @return nummer
  **/
  @JsonProperty("nummer")
  public String getNummer() {
    return nummer;
  }


 /**
   * Der Straßenname
   * @return strasse
  **/
  @JsonProperty("strasse")
  public String getStrasse() {
    return strasse;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Leerungen {\n");
    
    sb.append("    blau: ").append(toIndentedString(blau)).append("\n");
    sb.append("    braun: ").append(toIndentedString(braun)).append("\n");
    sb.append("    gelb: ").append(toIndentedString(gelb)).append("\n");
    sb.append("    grau: ").append(toIndentedString(grau)).append("\n");
    sb.append("    haId: ").append(toIndentedString(haId)).append("\n");
    sb.append("    nummer: ").append(toIndentedString(nummer)).append("\n");
    sb.append("    strasse: ").append(toIndentedString(strasse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

