/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.model;

import io.swagger.annotations.ApiModel;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Informationen zu einer Hausnummer
 **/
@ApiModel(description="Informationen zu einer Hausnummer")
public class Hausnummer  {
  
  @ApiModelProperty(required = true, value = "Die Hausnummern ID")
 /**
   * Die Hausnummern ID  
  **/
  private Long id = null;

  @ApiModelProperty(required = true, value = "Die Hausnummer mit Zusatzen")
 /**
   * Die Hausnummer mit Zusatzen  
  **/
  private String nummer = null;

  @ApiModelProperty(value = "Der Straßenname")
 /**
   * Der Straßenname  
  **/
  private String strasse = null;
 /**
   * Die Hausnummern ID
   * @return id
  **/
  @JsonProperty("id")
  public Long getId() {
    return id;
  }


 /**
   * Die Hausnummer mit Zusatzen
   * @return nummer
  **/
  @JsonProperty("nummer")
  public String getNummer() {
    return nummer;
  }


 /**
   * Der Straßenname
   * @return strasse
  **/
  @JsonProperty("strasse")
  public String getStrasse() {
    return strasse;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Hausnummer {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    nummer: ").append(toIndentedString(nummer)).append("\n");
    sb.append("    strasse: ").append(toIndentedString(strasse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

