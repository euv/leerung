/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.model;

import io.swagger.annotations.ApiModel;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Datumsangabe zu einer Leerung
 **/
@ApiModel(description="Datumsangabe zu einer Leerung")
public class LeerungsDate  {
  
  @ApiModelProperty(required = true, value = "Datum der Leerung")
 /**
   * Datum der Leerung  
  **/
  private String leerung = null;

  @ApiModelProperty(value = "Datum der Orginalleerung")
 /**
   * Datum der Orginalleerung  
  **/
  private String orgLeerung = null;

  @ApiModelProperty(example = "false", required = true, value = "Ob die Leerung von der Orginalleerung abweicht")
 /**
   * Ob die Leerung von der Orginalleerung abweicht  
  **/
  private Boolean verschoben = null;
 /**
   * Datum der Leerung
   * @return leerung
  **/
  @JsonProperty("leerung")
  public String getLeerung() {
    return leerung;
  }


 /**
   * Datum der Orginalleerung
   * @return orgLeerung
  **/
  @JsonProperty("orgLeerung")
  public String getOrgLeerung() {
    return orgLeerung;
  }


 /**
   * Ob die Leerung von der Orginalleerung abweicht
   * @return verschoben
  **/
  @JsonProperty("verschoben")
  public Boolean isVerschoben() {
    return verschoben;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LeerungsDate {\n");
    
    sb.append("    leerung: ").append(toIndentedString(leerung)).append("\n");
    sb.append("    orgLeerung: ").append(toIndentedString(orgLeerung)).append("\n");
    sb.append("    verschoben: ").append(toIndentedString(verschoben)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

