/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.model;

import io.swagger.annotations.ApiModel;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
  * Datumsangabe zu den Leerungen bei einer Hausnummer mit Feiertagsverschiebung
 **/
@ApiModel(description="Datumsangabe zu den Leerungen bei einer Hausnummer mit Feiertagsverschiebung")
public class LeerungenFeiertagverschiebung  {
  
  @ApiModelProperty(example = "\"[{'verschoben': false, 'leerung': '2018-09-17'},{'verschoben': true, 'orgLeerung': '2017-04-19', 'leerung': '2018-10-15'}]\"", required = true, value = "Datumsliste der Leerungen der Blauen-Tonne")
 /**
   * Datumsliste der Leerungen der Blauen-Tonne  
  **/
  private List<LeerungsDate> blau = new ArrayList<LeerungsDate>();

  @ApiModelProperty(example = "\"[{'verschoben': false, 'leerung': '2018-09-17'},{'verschoben': true, 'orgLeerung': '2017-04-19', 'leerung': '2018-10-15'}]\"", required = true, value = "Datumsliste der Leerungen der Braunen-Tonne")
 /**
   * Datumsliste der Leerungen der Braunen-Tonne  
  **/
  private List<LeerungsDate> braun = new ArrayList<LeerungsDate>();

  @ApiModelProperty(example = "\"[{'verschoben': false, 'leerung': '2018-09-17'},{'verschoben': false, 'leerung': '2018-10-15'}]\"", required = true, value = "Datumsliste der Leerungen der Gelben-Tonne")
 /**
   * Datumsliste der Leerungen der Gelben-Tonne  
  **/
  private List<LeerungsDate> gelb = new ArrayList<LeerungsDate>();

  @ApiModelProperty(example = "\"[{'verschoben': false, 'leerung': '2018-09-17'},{'verschoben': false, 'leerung': '2018-10-15'}]\"", required = true, value = "Datumsliste der Leerungen der Grauen-Tonne")
 /**
   * Datumsliste der Leerungen der Grauen-Tonne  
  **/
  private List<LeerungsDate> grau = new ArrayList<LeerungsDate>();

  @ApiModelProperty(value = "Die Hausnummer ID")
 /**
   * Die Hausnummer ID  
  **/
  private Long haId = null;

  @ApiModelProperty(value = "Die Hausnummer mit Zusatz")
 /**
   * Die Hausnummer mit Zusatz  
  **/
  private String nummer = null;

  @ApiModelProperty(value = "Der Straßenname")
 /**
   * Der Straßenname  
  **/
  private String strasse = null;
 /**
   * Datumsliste der Leerungen der Blauen-Tonne
   * @return blau
  **/
  @JsonProperty("blau")
  public List<LeerungsDate> getBlau() {
    return blau;
  }

  public void setBlau(List<LeerungsDate> blau) {
    this.blau = blau;
  }

  public LeerungenFeiertagverschiebung blau(List<LeerungsDate> blau) {
    this.blau = blau;
    return this;
  }

  public LeerungenFeiertagverschiebung addBlauItem(LeerungsDate blauItem) {
    this.blau.add(blauItem);
    return this;
  }

 /**
   * Datumsliste der Leerungen der Braunen-Tonne
   * @return braun
  **/
  @JsonProperty("braun")
  public List<LeerungsDate> getBraun() {
    return braun;
  }

  public void setBraun(List<LeerungsDate> braun) {
    this.braun = braun;
  }

  public LeerungenFeiertagverschiebung braun(List<LeerungsDate> braun) {
    this.braun = braun;
    return this;
  }

  public LeerungenFeiertagverschiebung addBraunItem(LeerungsDate braunItem) {
    this.braun.add(braunItem);
    return this;
  }

 /**
   * Datumsliste der Leerungen der Gelben-Tonne
   * @return gelb
  **/
  @JsonProperty("gelb")
  public List<LeerungsDate> getGelb() {
    return gelb;
  }

  public void setGelb(List<LeerungsDate> gelb) {
    this.gelb = gelb;
  }

  public LeerungenFeiertagverschiebung gelb(List<LeerungsDate> gelb) {
    this.gelb = gelb;
    return this;
  }

  public LeerungenFeiertagverschiebung addGelbItem(LeerungsDate gelbItem) {
    this.gelb.add(gelbItem);
    return this;
  }

 /**
   * Datumsliste der Leerungen der Grauen-Tonne
   * @return grau
  **/
  @JsonProperty("grau")
  public List<LeerungsDate> getGrau() {
    return grau;
  }

  public void setGrau(List<LeerungsDate> grau) {
    this.grau = grau;
  }

  public LeerungenFeiertagverschiebung grau(List<LeerungsDate> grau) {
    this.grau = grau;
    return this;
  }

  public LeerungenFeiertagverschiebung addGrauItem(LeerungsDate grauItem) {
    this.grau.add(grauItem);
    return this;
  }

 /**
   * Die Hausnummer ID
   * @return haId
  **/
  @JsonProperty("haId")
  public Long getHaId() {
    return haId;
  }


 /**
   * Die Hausnummer mit Zusatz
   * @return nummer
  **/
  @JsonProperty("nummer")
  public String getNummer() {
    return nummer;
  }


 /**
   * Der Straßenname
   * @return strasse
  **/
  @JsonProperty("strasse")
  public String getStrasse() {
    return strasse;
  }



  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LeerungenFeiertagverschiebung {\n");
    
    sb.append("    blau: ").append(toIndentedString(blau)).append("\n");
    sb.append("    braun: ").append(toIndentedString(braun)).append("\n");
    sb.append("    gelb: ").append(toIndentedString(gelb)).append("\n");
    sb.append("    grau: ").append(toIndentedString(grau)).append("\n");
    sb.append("    haId: ").append(toIndentedString(haId)).append("\n");
    sb.append("    nummer: ").append(toIndentedString(nummer)).append("\n");
    sb.append("    strasse: ").append(toIndentedString(strasse)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

