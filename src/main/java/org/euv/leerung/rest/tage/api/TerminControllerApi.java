/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.tage.api;

import java.util.List;
import javax.ws.rs.*;

import org.euv.leerung.rest.tage.model.Termin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

/**
 * REST API für Sondertage beim EUV
 *
 * <p>Liefert alle Feiertage so wie sonstige Aktionstage beim EUV.
 *
 */
@Path("/")
@Api(value = "/")
public interface TerminControllerApi  {

    /**
     * Informationen für einen Termin
     *
     */
    @GET
    @Path("/termin/get/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Informationen für einen Termin", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Termin.class),})
    public Termin listByIdUsingGET(@PathParam("id") Integer id);

    /**
     * Liste aller Termine für eine Kategorie
     *
     */
    @GET
    @Path("/termin/kategorie/{katId}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste aller Termine für eine Kategorie", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Termin.class, responseContainer = "array"),})
    public List<Termin> listByKatIdUsingGET(@PathParam("katId") Integer katId);

    /**
     * Liste aller Termine für einen Typ
     *
     */
    @GET
    @Path("/termin/typ/{typ}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste aller Termine für einen Typ", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Termin.class, responseContainer = "array"),})
    public List<Termin> listByTypUsingGET1(@PathParam("typ") String typ);

    /**
     * Listet aller Termine auf
     *
     */
    @GET
    @Path("/termin/list")
    @Produces({ "application/json" })
    @ApiOperation(value = "Listet aller Termine auf", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Termin.class, responseContainer = "array"),})
    public List<Termin> listUsingGET1();
}

