/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.tage.model;


import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Kategorie  {
  
  @ApiModelProperty(value = "Eine Beschreibung der Kategorie")
 /**
   * Eine Beschreibung der Kategorie  
  **/
  private String bezeichnung = null;
  @ApiModelProperty(value = "Der PK der Kategorie")
 /**
   * Der PK der Kategorie  
  **/
  private Integer id = null;
  @ApiModelProperty(value = "Der Typ der Kategorie")
 /**
   * Der Typ der Kategorie  
  **/
  private String typ = null;
  @ApiModelProperty(example = "false", value = "Ob es Verschiebungen geben kann")
 /**
   * Ob es Verschiebungen geben kann  
  **/
  private Boolean verschiebung = null;

 /**
   * Eine Beschreibung der Kategorie
   * @return bezeichnung
  **/
  @JsonProperty("bezeichnung")
  public String getBezeichnung() {
    return bezeichnung;
  }

  public void setBezeichnung(String bezeichnung) {
    this.bezeichnung = bezeichnung;
  }

  public Kategorie bezeichnung(String bezeichnung) {
    this.bezeichnung = bezeichnung;
    return this;
  }

 /**
   * Der PK der Kategorie
   * @return id
  **/
  @JsonProperty("id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Kategorie id(Integer id) {
    this.id = id;
    return this;
  }

 /**
   * Der Typ der Kategorie
   * @return typ
  **/
  @JsonProperty("typ")
  public String getTyp() {
    return typ;
  }

  public void setTyp(String typ) {
    this.typ = typ;
  }

  public Kategorie typ(String typ) {
    this.typ = typ;
    return this;
  }

 /**
   * Ob es Verschiebungen geben kann
   * @return verschiebung
  **/
  @JsonProperty("verschiebung")
  public Boolean isVerschiebung() {
    return verschiebung;
  }

  public void setVerschiebung(Boolean verschiebung) {
    this.verschiebung = verschiebung;
  }

  public Kategorie verschiebung(Boolean verschiebung) {
    this.verschiebung = verschiebung;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Kategorie {\n");
    
    sb.append("    bezeichnung: ").append(toIndentedString(bezeichnung)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    typ: ").append(toIndentedString(typ)).append("\n");
    sb.append("    verschiebung: ").append(toIndentedString(verschiebung)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

