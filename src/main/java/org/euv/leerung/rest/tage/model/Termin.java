/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.tage.model;


import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Termin  {
  
  @ApiModelProperty(value = "Beschreibung des Termins")
 /**
   * Beschreibung des Termins  
  **/
  private String bezeichnung = null;
  @ApiModelProperty(value = "Datum des Termins")
 /**
   * Datum des Termins  
  **/
  private String datum = null;
  @ApiModelProperty(value = "Das Icon welches genutzt werden soll")
 /**
   * Das Icon welches genutzt werden soll  
  **/
  private String icon = null;
  @ApiModelProperty(value = "Der PK des Termins")
 /**
   * Der PK des Termins  
  **/
  private Integer id = null;
  @ApiModelProperty(value = "Die Kategorie ID")
 /**
   * Die Kategorie ID  
  **/
  private Integer katId = null;
  @ApiModelProperty(value = "Der Typ der Kategorie")
 /**
   * Der Typ der Kategorie  
  **/
  private String typ = null;
  @ApiModelProperty(value = "Wie sich die Verschiebung äussert")
 /**
   * Wie sich die Verschiebung äussert  
  **/
  private Integer verschiebung = null;
  
  private String abkuerzung = null;

 /**
   * Beschreibung des Termins
   * @return bezeichnung
  **/
  @JsonProperty("bezeichnung")
  public String getBezeichnung() {
    return bezeichnung;
  }

  public void setBezeichnung(String bezeichnung) {
    this.bezeichnung = bezeichnung;
  }

  public Termin bezeichnung(String bezeichnung) {
    this.bezeichnung = bezeichnung;
    return this;
  }

 /**
   * Datum des Termins
   * @return datum
  **/
  @JsonProperty("datum")
  public String getDatum() {
    return datum;
  }

  public void setDatum(String datum) {
    this.datum = datum;
  }

  public Termin datum(String datum) {
    this.datum = datum;
    return this;
  }

 /**
   * Das Icon welches genutzt werden soll
   * @return icon
  **/
  @JsonProperty("icon")
  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Termin icon(String icon) {
    this.icon = icon;
    return this;
  }

 /**
   * Der PK des Termins
   * @return id
  **/
  @JsonProperty("id")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Termin id(Integer id) {
    this.id = id;
    return this;
  }

 /**
   * Die Kategorie ID
   * @return katId
  **/
  @JsonProperty("katId")
  public Integer getKatId() {
    return katId;
  }

  public void setKatId(Integer katId) {
    this.katId = katId;
  }

  public Termin katId(Integer katId) {
    this.katId = katId;
    return this;
  }

 /**
   * Der Typ der Kategorie
   * @return typ
  **/
  @JsonProperty("typ")
  public String getTyp() {
    return typ;
  }

  public void setTyp(String typ) {
    this.typ = typ;
  }

  public Termin typ(String typ) {
    this.typ = typ;
    return this;
  }

 /**
   * Wie sich die Verschiebung äussert
   * @return verschiebung
  **/
  @JsonProperty("verschiebung")
  public Integer getVerschiebung() {
    return verschiebung;
  }

  public void setVerschiebung(Integer verschiebung) {
    this.verschiebung = verschiebung;
  }

  public Termin verschiebung(Integer verschiebung) {
    this.verschiebung = verschiebung;
    return this;
  }
  
  @JsonProperty("abkuerzung")
  public String getAbkuerzung() {
	  return abkuerzung;
  }
  public void setAbkuerzung(String abkuerzung) {
	  this.abkuerzung = abkuerzung;
  }
  public Termin abkuerzung(String abkuerzung) {
	  this.abkuerzung = abkuerzung;
	  return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Termin {\n");
    
    sb.append("    bezeichnung: ").append(toIndentedString(bezeichnung)).append("\n");
    sb.append("    datum: ").append(toIndentedString(datum)).append("\n");
    sb.append("    icon: ").append(toIndentedString(icon)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    katId: ").append(toIndentedString(katId)).append("\n");
    sb.append("    typ: ").append(toIndentedString(typ)).append("\n");
    sb.append("    verschiebung: ").append(toIndentedString(verschiebung)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

