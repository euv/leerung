/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.rest.tage.api;

import java.util.List;
import javax.ws.rs.*;

import org.euv.leerung.rest.tage.model.Kategorie;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;

/**
 * REST API für Sondertage beim EUV
 *
 * <p>Liefert alle Feiertage so wie sonstige Aktionstage beim EUV.
 *
 */
@Path("/")
@Api(value = "/")
public interface KategorienControllerApi  {

    /**
     * Information für eine Kategorie
     *
     */
    @GET
    @Path("/kategorien/get/{id}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Information für eine Kategorie", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Kategorie.class),})
    public Kategorie getByIdUsingGET(@PathParam("id") Integer id);

    /**
     * Liste der Kategorien für einen bestimmten Typ
     *
     */
    @GET
    @Path("/kategorien/typ/{typ}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste der Kategorien für einen bestimmten Typ", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Kategorie.class, responseContainer = "array"),})
    public List<Kategorie> listByTypUsingGET(@PathParam("typ") String typ);

    /**
     * Liste der Kategorien ob mit Verschiebung oder ohne
     *
     */
    @GET
    @Path("/kategorien/verschiebungen/{verschiebung}")
    @Produces({ "application/json" })
    @ApiOperation(value = "Liste der Kategorien ob mit Verschiebung oder ohne", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Kategorie.class, responseContainer = "array"),})
    public List<Kategorie> listByVerschiebungenUsingGET(@PathParam("verschiebung") Boolean verschiebung);

    /**
     * Listet alle Kategorien auf
     *
     */
    @GET
    @Path("/kategorien/list")
    @Produces({ "application/json" })
    @ApiOperation(value = "Listet alle Kategorien auf", tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "OK", response = Kategorie.class, responseContainer = "array"),})
    public List<Kategorie> listUsingGET();
}

