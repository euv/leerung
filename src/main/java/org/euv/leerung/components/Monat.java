/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.components;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.euv.leerung.rest.model.LeerungenFeiertagverschiebung;
import org.euv.leerung.rest.model.LeerungsDate;
import org.joda.time.DateTimeConstants;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

/**
 * A Component to render the Lerrungen for a hausId for a month. 
 * @author Marcus Schneider
 */
@Import(stylesheet="context:css/monat.less")
@Component
public class Monat {
	
	@Inject
	private Logger logger;

	@Property
	private List<Long> weeks;
	
	@Property
	private Long week;
	
	@Property
	private List<Long> days;
	
	@Property
	private Long day;
	
	@Property
	@Parameter(required=true, allowNull=false)
	private Long month;
	
	@Property
	@Parameter(required=true, allowNull=false)
	private Long year;
	
	@Property
	@Parameter(required=true, allowNull=false)
	private LeerungenFeiertagverschiebung leerungen;
	
	@Property
	@Parameter(value="true")
	private Boolean fillWeeks;
	
	
	private List<LocalDate> listGrau;
	private List<LocalDate> listBraun;
	private List<LocalDate> listGelb;
	private List<LocalDate> listBlau;
	
	private LocalDate[][] allDaysArray = new LocalDate[6][7];
	
	private LocalDate start;
	private Interval interval;
	
	private static final String[] MONATS_NAMEN = {
			"Januar",
			"Februar",
			"März",
			"April",
			"Mai",
			"Juni",
			"Juli",
			"August",
			"September",
			"Oktober",
			"November",
			"Dezember"
	};
	
	private static final String[] TAGES_NAMEN= {
			"Mo",
			"Di",
			"Mi",
			"Do",
			"Fr",
			"Sa",
			"So"
	};
	

	/**
	 * Setup for the component.
	 * 
	 * Request the datas from the API.
	 */
	void setupRender() {
		start = new LocalDate(year.intValue(), month.intValue(), 1);

		LocalDate end = start.plusMonths(1);

		interval = new Interval(start.toDateTimeAtStartOfDay(), end.toDateTimeAtStartOfDay());

		logger.debug("Start to End (interval): {} / {} ({})", start, end, interval);

		initLeerungslisten();
		initDateArray();
	}

	/**
	 * Fills the array for displaying per day and week.
	 */
	private void initDateArray() {
		LocalDate next = start.withDayOfWeek(DateTimeConstants.MONDAY);
		int weekcounts = 0;
		for(int weekLoop=0; weekLoop<6; ++weekLoop) {
			boolean set = false;
			for(int dayLoop=0; dayLoop<7; ++dayLoop) {
				if(interval.contains(next.toDateTimeAtStartOfDay()) && !set) {
					++weekcounts;
					set = true;
				}
				allDaysArray[weekLoop][dayLoop] = next;
				logger.trace("fill LD[{}][{}] = {}", weekLoop, dayLoop, next);
				next = next.plusDays(1);
			}
		}

		logger.debug("FillWeeks: {}", fillWeeks);
		
		initLoops(fillWeeks ? 6 : weekcounts);
	}

	/**
	 * Fills the property for looping in the template.
	 * @param weekscount How many week should be displayed 
	 */
	private void initLoops(int weekscount) {
		weeks = new ArrayList<>();
		for(int a=1; a<=weekscount; ++a) {
			weeks.add(Long.valueOf(a));
		}
		
		days = new ArrayList<>();
		for(int a=1; a<=7; ++a) {
			days.add(Long.valueOf(a));
		}
	}
	
	/**
	 * Fills the display array with all Leerungen.
	 */
	private void initLeerungslisten() {
		listBlau = new ArrayList<>();
		for(LeerungsDate leerung : leerungen.getBlau()) {
			listBlau.add(LocalDate.parse(leerung.getLeerung()));
		}

		listGrau = new ArrayList<>();
		for(LeerungsDate leerung : leerungen.getGrau()) {
			listGrau.add(LocalDate.parse(leerung.getLeerung()));
		}

		listGelb = new ArrayList<>();
		for(LeerungsDate leerung : leerungen.getGelb()) {
			listGelb.add(LocalDate.parse(leerung.getLeerung()));
		}

		listBraun = new ArrayList<>();
		for(LeerungsDate leerung : leerungen.getBraun()) {
			listBraun.add(LocalDate.parse(leerung.getLeerung()));
		}
	}
	
	public String getDateString() {
		LocalDate ld = allDaysArray[week.intValue()-1][day.intValue()-1];
		return ld.toString("dd");
	}
	
	public String getWeekendClass() {
		LocalDate ld = allDaysArray[week.intValue()-1][day.intValue()-1];
		if(ld.getDayOfWeek()==DateTimeConstants.SATURDAY || ld.getDayOfWeek()==DateTimeConstants.SUNDAY) {
			return "euv-monat-weekend";
		}
		return "euv-monat-nonweekend";
	}
	
	public String getColnum() {
		return "euv-monat-col-"+ Integer.toString(day.intValue());
	}
	
	public String getWertGrau() {
		Long a = week * day;
		return a.toString();
	}
	
	public String getMonthname() {
		return MONATS_NAMEN[month.intValue()-1];
	}
	
	public String getDayname() {
		return TAGES_NAMEN[day.intValue()-1];
	}
	
	public Boolean getIsInMonth() {
		LocalDate ld = allDaysArray[week.intValue()-1][day.intValue()-1];
		return interval.contains(ld.toDateTimeAtStartOfDay());
	}
	
	public Boolean getIsGrau() {
		LocalDate ld = allDaysArray[week.intValue()-1][day.intValue()-1];
		return listGrau.stream().anyMatch(ld::equals);
	}

	public Boolean getIsGelb() {
		LocalDate ld = allDaysArray[week.intValue()-1][day.intValue()-1];
		return listGelb.stream().anyMatch(ld::equals);
	}

	public Boolean getIsBlau() {
		LocalDate ld = allDaysArray[week.intValue()-1][day.intValue()-1];
		return listBlau.stream().anyMatch(ld::equals);
	}

	public Boolean getIsBraun() {
		LocalDate ld = allDaysArray[week.intValue()-1][day.intValue()-1];
		return listBraun.stream().anyMatch(ld::equals);
	}
}
