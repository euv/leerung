/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.services;

import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.renderer.CellRenderer;
import com.itextpdf.layout.renderer.DrawContext;

public class UpDownCellRenderer extends CellRenderer{

	public UpDownCellRenderer(Cell modelElement) {
		super(modelElement);
	}

	@Override
	public void draw(DrawContext drawContext) {
		super.draw(drawContext);
		
		PdfCanvas canvas = drawContext.getCanvas();
		Rectangle position = getOccupiedAreaBBox();
		canvas.saveState();
		
		canvas.moveTo(position.getLeft(), position.getTop());
		canvas.lineTo(position.getRight(), position.getTop());
		canvas.lineTo(position.getRight(), position.getBottom());
		canvas.lineTo(position.getLeft(), position.getBottom());
		canvas.clip();
		
		canvas.newPath();
		canvas.setLineWidth(1f);
		canvas.setColor(DeviceRgb.BLACK, false);
		canvas.moveTo(position.getRight(), position.getBottom());
		canvas.lineTo(position.getLeft(), position.getBottom());
		canvas.stroke();
		
		canvas.newPath();
		canvas.setColor(DeviceRgb.WHITE, false);
		canvas.setLineWidth(PdfHelper.mmInValue(1));
		canvas.moveTo(position.getLeft(), position.getTop());
		canvas.lineTo(position.getLeft(), position.getBottom());
		canvas.stroke();

		canvas.newPath();
		canvas.setColor(DeviceRgb.WHITE, false);
		canvas.setLineWidth(PdfHelper.mmInValue(1));
		canvas.moveTo(position.getRight(), position.getTop());
		canvas.lineTo(position.getRight(), position.getBottom());
		canvas.stroke();

		
		canvas.restoreState();
	}
}
