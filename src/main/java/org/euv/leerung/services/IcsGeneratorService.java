/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.euv.leerung.rest.api.LeerungApi;
import org.euv.leerung.rest.model.LeerungenFeiertagverschiebung;
import org.euv.leerung.rest.model.LeerungsDate;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Sequence;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Transp;
import net.fortuna.ical4j.model.property.TzId;
import net.fortuna.ical4j.model.property.Url;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.util.FixedUidGenerator;

/**
 * Generates the ICS file.
 * @author Marcus Schneider
 *
 */
public class IcsGeneratorService {
	@Inject
	private Logger logger;

	@Inject
	@Autowired
	private LeerungApi leerungApi;
	
	private static final String HOMEPAGE_URL = "https://www.euv-stadtbetrieb.de";
	
	int aSequenceNo;

	public InputStream generateIcs(long hausId) throws IOException {
		LeerungenFeiertagverschiebung leerungen;
		try {
			leerungen = leerungApi.getLeerungFeiertagsverschiebungByHaIdUsingGET(hausId, true);
		}
		catch(Exception e) {
			return null;
		}
		
		Calendar icalCalendar = new Calendar();
		icalCalendar.getProperties().add(new ProdId("-//EUV Stadtbetrieb Castrop-Rauxel -AöR-/Leerungen/DE"));
		icalCalendar.getProperties().add(Version.VERSION_2_0);
		icalCalendar.getProperties().add(CalScale.GREGORIAN);
		icalCalendar.getProperties().add(new TzId("Europe/Berlin"));
		icalCalendar.getProperties().add(new Summary("Leerung der Mülltonnen"));

		String ort = leerungen.getStrasse() +" "+ leerungen.getNummer();
		
		aSequenceNo = 0;
		leerungen.getGrau().stream().forEach(item -> {
			String summary = "EUV Abfuhr Restabfall";
			icalCalendar.getComponents().add(createCalEvent(item, summary, ort));
		});
		leerungen.getGelb().stream().forEach(item -> {
			String summary = "EUV Abfuhr Wertstoff";
			icalCalendar.getComponents().add(createCalEvent(item, summary, ort));
		});
		leerungen.getBraun().stream().forEach(item -> {
			String summary = "EUV Abfuhr Bioabfall";
			icalCalendar.getComponents().add(createCalEvent(item, summary, ort));
		});
		leerungen.getBlau().stream().forEach(item -> {
			String summary = "EUV Abfuhr Altpapier";
			icalCalendar.getComponents().add(createCalEvent(item, summary, ort));
		});
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		CalendarOutputter co = new CalendarOutputter();
		co.output(icalCalendar, baos);

		return new ByteArrayInputStream(baos.toByteArray());
	}

	private VEvent createCalEvent(LeerungsDate item, String summary, String ort) {
		FixedUidGenerator uid;
		try {
			uid = new FixedUidGenerator("1");
		} catch (SocketException e1) {
			uid = null;
		}
		
		VEvent vevent = new VEvent();
		LocalDate ld = new LocalDate(item.getLeerung());
		logger.debug("D Grau: {}", ld);
		Date dateForEvent = new Date(ld.toDate());
		vevent.getProperties().add(new DtStart(dateForEvent));
		vevent.getProperties().add(new Summary(summary));
		if(uid!=null) {
			vevent.getProperties().add(uid.generateUid());
		}
		try {
			vevent.getProperties().add(new Url(new URI(HOMEPAGE_URL)));
		} catch (URISyntaxException e) {
			logger.error("Catch URI syntax error", e);
		}
		vevent.getProperties().add(new Description("EUV Stadtbetrieb Castrop-Rauxel\nWestring 215\n44575 Castrop-Rauxel"));
		vevent.getProperties().add(new Sequence(aSequenceNo));
		vevent.getProperties().add(new Transp("TRANSPARENT"));
		vevent.getProperties().add(new Location(ort));
		++aSequenceNo;
		
		return vevent;
	}
}
