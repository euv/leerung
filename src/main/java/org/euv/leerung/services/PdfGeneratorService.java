/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.services;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;

import org.apache.tapestry5.ioc.annotations.Inject;
import org.euv.leerung.rest.api.LeerungApi;
import org.euv.leerung.rest.model.LeerungenFeiertagverschiebung;
import org.euv.leerung.rest.model.LeerungsDate;
import org.euv.leerung.rest.tage.api.KategorienControllerApi;
import org.euv.leerung.rest.tage.api.TerminControllerApi;
import org.euv.leerung.rest.tage.model.Termin;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfNumber;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfViewerPreferences;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.Style;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.AreaBreak;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Tab;
import com.itextpdf.layout.element.TabStop;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.property.TabAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import com.itextpdf.layout.property.VerticalAlignment;
import com.itextpdf.io.font.FontProgramFactory;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.DeviceRgb;

/**
 * The PDF generation service
 * @author Marcus Schneider
 */
public class PdfGeneratorService {
	
	private static final float ICON_X = 4.0f;
	private static final float ICON_Y = 9f;
	private static final String IMAGE_RESTABFALL = "restabfall";
	private static final String IMAGE_BIOABFALL = "bioabfall";
	private static final String IMAGE_WERTSTOFFE = "wertstoffe";
	private static final String IMAGE_ALTPAPIER = "altpapier";
	private static final String IMAGE_TANNENBAUM = "tannenbaumsammlung";
	
	@Inject
	Logger logger;

	@Inject
	@Autowired
	private LeerungApi leerungApi;
	
	@Inject
	@Autowired
	private KategorienControllerApi kategorienControllerApi;
	
	@Inject
	@Autowired
	private TerminControllerApi terminControllerApi;
	
	@Inject
	@Autowired
	private ServletContext servletContext;

	static final PdfNumber INVERTEDPORTRAIT = new PdfNumber(180);
	static final PdfNumber LANDSCAPE = new PdfNumber(90);
	static final PdfNumber PORTRAIT = new PdfNumber(0);
	static final PdfNumber SEASCAPE = new PdfNumber(270);
 
	PdfDocument pdfDoc;
	Document doc;
	
	int year;
	long hausId;
	LeerungenFeiertagverschiebung leerungen;
	List<Termin> listFeiertage;
	List<Termin> listAktionstage;

	Style styleNoPaddingMargins = new Style();
	
	public PdfGeneratorService() {
		super();
	}
	
	public InputStream generatePdf(int year, long hausId) throws IOException {
		logger.debug("year / hausId: {} / {}", year, hausId);
		
		this.year = year;
		this.hausId = hausId;
		leerungen = leerungApi.getLeerungFeiertagsverschiebungByHaIdUsingGET(hausId, true);
		try {
			listFeiertage = terminControllerApi.listByTypUsingGET1("FE");
		} catch(Exception e) {
		}
		try {
			listAktionstage = terminControllerApi.listByTypUsingGET1("AT");
		} catch(Exception e) {
		}
		
		
		ByteArrayOutputStream byos = new ByteArrayOutputStream();
		
		PdfWriter pdfWriter = new PdfWriter(byos);
		pdfDoc = new PdfDocument(pdfWriter);
		pdfDoc.setTagged();
		pdfDoc.getCatalog().setViewerPreferences(new PdfViewerPreferences().setDisplayDocTitle(true));
		pdfDoc.getCatalog().setLang(new PdfString("de-DE"));
		PdfDocumentInfo info = pdfDoc.getDocumentInfo();
		info.setAuthor("EUV Stadtbetrieb Castrop-Rauxel -AöR-");
		info.setTitle("Leerungstermine "+ Integer.toString(year) +" - "+ leerungen.getStrasse() +" "+ leerungen.getNummer());
		
		Set<String> s = FontProgramFactory.getRegisteredFonts();
		for (String string : s) {
			logger.debug("Font: {}", string);
		}

		doc = new Document(pdfDoc, PageSize.A4.rotate());
		doc.setMargins(PdfHelper.mmInValue(10),PdfHelper.mmInValue(15),PdfHelper.mmInValue(0),PdfHelper.mmInValue(15));
		
		String fontPath = servletContext.getRealPath("/WEB-INF/fonts/");
		PdfFontFactory.registerDirectory(fontPath);
		PdfFont pdfFontHelvetica = PdfFontFactory.createRegisteredFont("helvetica", null, true);
		
		logger.debug("FONT: {}", pdfFontHelvetica);
		doc.setFont(pdfFontHelvetica);
		
		initStyles();
		
		String[] monate1 = {
				"JANUAR",
				"FEBRUAR",
				"MÄRZ",
				"APRIL",
				"MAI",
				"JUNI"
		};
		logger.debug("Begin render page 1");
		renderPage(doc, monate1, 1);
		logger.debug("End render page 1");
		
		doc.add(new AreaBreak(PageSize.A4.rotate()));
		String[] monate2 = {
				"JULI",
				"AUGUST",
				"SEPTEMBER",
				"OKTOBER",
				"NOVEMBER",
				"DEZEMBER"
		};
		logger.debug("Begin render page 2");
		renderPage(doc, monate2, 7);
		logger.debug("End render page 2");

		doc.close();
		logger.debug("Closed doc");
		
		return new ByteArrayInputStream(byos.toByteArray());
	}

	private void initStyles() {
		styleNoPaddingMargins.setPadding(0);
		styleNoPaddingMargins.setMargin(0);
		styleNoPaddingMargins.setBorder(Border.NO_BORDER);
	}

	private void renderPage(Document doc, String[] monate, int start) throws MalformedURLException {
		float width = doc.getPageEffectiveArea(PageSize.A4.rotate()).getWidth();
		float cellWidth = width / 6;

		Style styleMonatsnamen = new Style();
		styleMonatsnamen.setBold();

		Style styleColHeader = new Style();
		styleColHeader.setBackgroundColor(new DeviceRgb(191, 190, 188));
		styleColHeader.setBorder(new SolidBorder(DeviceRgb.WHITE, PdfHelper.mmInValue(1)));

		doc.add(createPageHeaderLine(doc));

		// First row (header)
		Table table = new Table(new float[] {cellWidth,cellWidth,cellWidth,cellWidth,cellWidth,cellWidth});
		for(String m : monate) {
			Text text = new Text(m);
			text.addStyle(styleMonatsnamen);
			
			Cell c = new Cell().add(new Paragraph(text));
			c.addStyle(styleColHeader);
			c.setTextAlignment(TextAlignment.CENTER);
			table.addCell(c);
		}
		
		// Data in cols
		for(int m=start; m<start+6; ++m) {
			Table tableMonth = renderMonth(m, cellWidth);
			
			Cell c = new Cell();
			c.addStyle(styleNoPaddingMargins);
			c.add(tableMonth);
			
			table.addCell(c);
		}
		doc.add(table);
		
		doc.add(createPageFooterLine(doc));
	}
	
	private Cell createPageHeaderLine(Document doc) {
		Cell c = new Cell();
		c.addStyle(styleNoPaddingMargins);
		c.setHeight(PdfHelper.mmInValue(13.0f));
		
		TabStop[] tabStops = {
				new TabStop(PdfHelper.mmInValue(45.0f), TabAlignment.LEFT),
				new TabStop(PdfHelper.mmInValue(180.0f), TabAlignment.LEFT)
		};
		Paragraph p = new Paragraph();
		p.addTabStops(tabStops);
		try {
			String stringLogo = servletContext.getRealPath("/WEB-INF/icons/logo.png");
			Image imgLogo = new Image(ImageDataFactory.create(stringLogo));
			imgLogo.scaleToFit(PdfHelper.mmInValue(30.0f), PdfHelper.mmInValue(10.0f));
			p.add(imgLogo);
		} catch (MalformedURLException e) {
			logger.error("Malformed URL in Image creation", e);
		}
		
		Style styleTitle1 = new Style();
		styleTitle1.setBorder(Border.NO_BORDER);
		styleTitle1.setFontSize(11.0f);
		styleTitle1.setBold();
		Text textTitle1 = new Text("ABFALLKALENDER "+ Integer.toString(year));
		textTitle1.addStyle(styleTitle1);
		
		Style styleTitle2 = new Style();
		styleTitle2.setBorder(Border.NO_BORDER);
		styleTitle2.setFontSize(7.0f);
		Text textTitle2 = new Text("Verschiebung der Leerungstermine durch Feiertage sind berücksichtigt.");
		textTitle2.addStyle(styleTitle2);
		
		Cell cellText1 = new Cell();
		cellText1.add(new Paragraph(textTitle1));
		cellText1.add(new Paragraph(textTitle2));
		p.add(new Tab());
		p.add(cellText1);

		
		
		Style styleTitle3 = new Style();
		styleTitle3.setBorder(Border.NO_BORDER);
		styleTitle3.setFontSize(9.0f);
		styleTitle3.setBold();
		Text textTitle3 = new Text("IHR STANDORT: "+ leerungen.getStrasse() +" "+ leerungen.getNummer());
		textTitle3.addStyle(styleTitle3);
		
		Style styleTitle4 = new Style();
		styleTitle4.setBorder(Border.NO_BORDER);
		styleTitle4.setFontSize(7.0f);
		LocalDate ld = LocalDate.now();
		Text textTitle4 = new Text("Stand: "+ ld.toString("dd.MM.yyyy") +" / Bitte überprüfen Sie regelmäßig Ihre Abfuhrtermine!");
		textTitle4.addStyle(styleTitle4);
		
		Cell cellText2 = new Cell();
		cellText2.add(new Paragraph(textTitle3));
		cellText2.add(new Paragraph(textTitle4));
		p.add(new Tab());
		p.add(cellText2);

		c.add(p);
		return c;
	}
	
	
	
	private Cell createPageFooterLine(Document doc) {
		TabStop[] tabStops = {
				new TabStop(10),
				new TabStop(70),
				new TabStop(130),
				new TabStop(190),
				new TabStop(250),
		};
		
		Style styleFootLine = new Style();
		styleFootLine.setMargin(0);
		styleFootLine.setPadding(0);
		styleFootLine.setFontSize(7.5f);
		
		Cell cellLeft1 = new Cell();
		cellLeft1.addStyle(styleNoPaddingMargins);
		
		Style styleTonne = new Style();
		styleTonne.setPadding(0);
		styleTonne.setMargin(0);
		styleTonne.setMarginRight(PdfHelper.mmInValue(2));
		styleTonne.setBorder(Border.NO_BORDER);
		
		Paragraph pImages = new Paragraph();
		pImages.addStyle(styleFootLine);
		pImages.addTabStops(tabStops);
		
		addImageToSubscribtion(styleTonne, pImages, IMAGE_RESTABFALL, "Restabfall");
		addImageToSubscribtion(styleTonne, pImages, IMAGE_WERTSTOFFE, "Wertstoffe");
		addImageToSubscribtion(styleTonne, pImages, IMAGE_BIOABFALL, "Bioabfall");
		addImageToSubscribtion(styleTonne, pImages, IMAGE_ALTPAPIER, "Altpapier");
		addImageToSubscribtion(styleTonne, pImages, IMAGE_TANNENBAUM, "Weihnachtsbaumsammlung");
		cellLeft1.add(pImages);
		
		Cell cellLeft2 = new Cell();
		cellLeft2.addStyle(styleNoPaddingMargins);
		
		Paragraph pEmail = new Paragraph();
		pEmail.addTabStops(tabStops);
		pEmail.addStyle(styleFootLine);
		pEmail.setBold();
		pEmail.add(new Tab());
		pEmail.add("Weitere Infos unter: www.euv-stadtbetrieb.de");
		cellLeft2.add(pEmail);

		
		
		Cell cellRight1 = new Cell();
		cellRight1.addStyle(styleNoPaddingMargins);
		
		Paragraph pFragen = new Paragraph();
		pFragen.addStyle(styleFootLine);
		Text t1 = new Text("Sie haben Fragen zur Leerung?    Tel.");
		t1.setBold();
		pFragen.add(t1);
		
		Text t2 = new Text(" 02305 9686 - 666");
		pFragen.add(t2);
		
		Text t3 = new Text("    E-Mail:");
		t3.setBold();
		pFragen.add(t3);
		
		Text t4 = new Text(" abfallwirtschaft@euv-stadtbetrieb.de");
		pFragen.add(t4);
		
		cellRight1.add(pFragen);
		
		
		Cell cellRight2 = new Cell();
		cellRight2.addStyle(styleNoPaddingMargins);

		Paragraph pTermine = new Paragraph();
		pTermine.addStyle(styleFootLine);
		Text t5 = new Text("Termine "+ Integer.toString(year) +": ");
		t5.setBold();
		pTermine.add(t5);
		try {
			pTermine.add(createTermine());
		}
		catch(Exception e) {}
		cellRight2.add(pTermine);
		
		
		
		UnitValue[] unitTable = {
				new UnitValue(UnitValue.PERCENT, 50),
				new UnitValue(UnitValue.PERCENT, 50)
		};
		Table table = new Table(unitTable);
		table.addCell(cellLeft1);
		table.addCell(cellRight1);
		table.addCell(cellLeft2);
		table.addCell(cellRight2);
		table.useAllAvailableWidth();
		
		Cell topCell = new Cell();
		topCell.addStyle(styleNoPaddingMargins);
		topCell.setPaddingTop(PdfHelper.mmInValue(2));
		topCell.add(table);

		return topCell;
	}
	
	
	
	private Text createTermine() {
		return new Text(terminControllerApi.listByTypUsingGET1("FZ")
				.stream()
				.map( x -> x.getBezeichnung())
				.collect(Collectors.joining(" / ")));
	}

	
	
	private Table renderMonth(int month, float cellWidth) throws MalformedURLException {
		String[] daynames = {
				"Mo",
				"Di",
				"Mi",
				"Do",
				"Fr",
				"Sa",
				"So"
		};
		
		Style styleRow = new Style();
		styleRow.setFontSize(9.0f);
		styleRow.setBackgroundColor(DeviceRgb.WHITE);
		styleRow.setPadding(0);
		styleRow.setMargin(0);
		styleRow.setPaddingLeft(5);
		styleRow.setPaddingRight(5);
		styleRow.setPaddingBottom(-1);
		styleRow.setHeight(PdfHelper.mmInValue(5.0f));
		styleRow.setBorder(Border.NO_BORDER);
		styleRow.setVerticalAlignment(VerticalAlignment.BOTTOM);
		
		Style[] styleWeekdays = {
				new Style(),
				new Style(),
				new Style(),
				new Style(),
				new Style(),
				new Style(),
				new Style()
		};
		styleWeekdays[6-1].setBackgroundColor(new DeviceRgb(213, 213, 209));
		styleWeekdays[7-1].setBackgroundColor(new DeviceRgb(213, 213, 209));

		Style styleFeiertag = new Style();
		styleFeiertag.setBackgroundColor(new DeviceRgb(202, 237, 246));
		Style styleFeiertagText = new Style();
		styleFeiertagText.setFontSize(6.0f);
		
		Style styleAktionstag = new Style();
		styleAktionstag.setBackgroundColor(new DeviceRgb(213, 213, 209));
		Style styleAktionstagText = new Style();
		styleAktionstagText.setFontSize(6.0f);

		Style styleTonne = new Style();
		styleTonne.setBorder(Border.NO_BORDER);
		styleTonne.setMargin(0);
		styleTonne.setPadding(0);
		styleTonne.setPaddingRight(PdfHelper.mmInValue(1));
		styleTonne.setMarginBottom(0);
		styleTonne.setMarginRight(PdfHelper.mmInValue(1.0f));
		styleTonne.setVerticalAlignment(VerticalAlignment.MIDDLE);

		Style styleBold = new Style();
		styleBold.setBold();

		Table table = new Table(1);
		table.setBorder(Border.NO_BORDER);
		table.useAllAvailableWidth();
		TabStop[] tabStops = {
				new TabStop(15, TabAlignment.RIGHT),
				new TabStop(20, TabAlignment.LEFT),
				new TabStop(42, TabAlignment.LEFT),
				new TabStop(cellWidth, TabAlignment.RIGHT),
		};
		/*
		 * Messup code... Ugly, but working!
		 * Must be refactored
		 */
		Image imageRestabfall = createImage(IMAGE_RESTABFALL);
		imageRestabfall.addStyle(styleTonne);
		Image imageWertstoffe = createImage(IMAGE_WERTSTOFFE);
		imageWertstoffe.addStyle(styleTonne);
		Image imageBioabfall = createImage(IMAGE_BIOABFALL);
		imageBioabfall.addStyle(styleTonne);
		Image imageAltpapier = createImage(IMAGE_ALTPAPIER);
		imageAltpapier.addStyle(styleTonne);

		for(int d=1; d<32; ++d) {
			Paragraph p = new Paragraph();
			Cell c = new Cell();
			c.addStyle(styleRow);
			
			try {
				LocalDate ld = new LocalDate(year, month, d);
				logger.trace("LD: {}", ld);

				Text dayname = new Text(daynames[ld.getDayOfWeek()-1]);
				Text day = new Text(String.format("%02d", d));
				day.addStyle(styleBold);
				
				c.addStyle(styleWeekdays[ld.getDayOfWeek()-1]);
				
				p.addTabStops(tabStops);
				
				p.add(new Tab()).add(day).add(new Tab()).add(dayname);
				p.add(new Tab());
				
				
				
				Predicate<LeerungsDate> matchLeerungsDate = x -> LocalDate.parse(x.getLeerung()).isEqual(ld);
				if(leerungen.getGrau().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageRestabfall);
				}
				if(leerungen.getGelb().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageWertstoffe);
				}
				if(leerungen.getBraun().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageBioabfall);
				}
				if(leerungen.getBlau().stream().anyMatch(matchLeerungsDate)) {
					p.add(imageAltpapier);
				}
				
				createAktionTermin(styleAktionstag, styleAktionstagText, styleTonne, p, c, ld);
				createFeiertagTermin(styleFeiertag, styleFeiertagText, p, c, ld);
			}
			catch(IllegalFieldValueException ifve) {
				logger.debug("Illegal field value found", ifve);
			}
			
			c.add(p);

			c.setNextRenderer(new UpDownCellRenderer(c));
			table.addCell(c);
		}
		
		return table;
	}

	private void createFeiertagTermin(Style styleFeiertag, Style styleFeiertagText, Paragraph p, Cell c, LocalDate ld) {
		Termin terminFeiertag = listFeiertage.stream().filter(x -> LocalDate.parse(x.getDatum()).isEqual(ld)).reduce(null, (a,b) -> b);
		if(terminFeiertag !=  null) {
			Text t = new Text(terminFeiertag.getBezeichnung() == null ? "" : terminFeiertag.getBezeichnung());
			t.addStyle(styleFeiertagText);
			p.add(t);
			
			c.addStyle(styleFeiertag);
		}
	}

	private void createAktionTermin(Style styleAktionstag, Style styleAktionstagText, Style styleTonne, Paragraph p, Cell c, LocalDate ld) throws MalformedURLException {
		Termin terminAktionstag = listAktionstage.stream().filter(x -> LocalDate.parse(x.getDatum()).isEqual(ld)).reduce(null, (a,b) -> b);
		if(terminAktionstag != null) {
			try {
				Image image = createImage(terminAktionstag.getIcon());
				image.addStyle(styleTonne);
				p.add(image);
			}
			catch(com.itextpdf.io.IOException ioe) {
				logger.debug("Catch exception for ({})", terminAktionstag.getIcon());
			}
			Text t = new Text(terminAktionstag.getBezeichnung() == null ? "" : terminAktionstag.getBezeichnung());
			t.addStyle(styleAktionstagText);
			p.add(t);

			c.addStyle(styleAktionstag);
		}
	}

	private Image createImage(String name) throws MalformedURLException, com.itextpdf.io.IOException {
		String file = servletContext.getRealPath("/WEB-INF/icons/"+ name +".png");
		Image image = new Image(ImageDataFactory.create(file));
		image.scaleToFit(PdfHelper.mmInValue(ICON_X), ICON_Y);
		return image;
	}

	private Paragraph addImageToSubscribtion(Style style, Paragraph p, String imageName, String text) {
		try {
			Image image = createImage(imageName);
			image.addStyle(style);
			p.add(new Tab());
			p.add(image);
			p.add(text);
		} catch (MalformedURLException e) {
			logger.error("Malformed URL in createImage", e);
		}
		return p;
	}
}
