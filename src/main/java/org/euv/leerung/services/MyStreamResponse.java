/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.services;

import java.io.IOException;
import java.io.InputStream;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.services.Response;

/**
 * Basic implementation for a stream response.
 * @author Marcus Schneider
 *
 */
public class MyStreamResponse implements StreamResponse {
	
	private InputStream inputStream;
	
	protected String contentType = "";
	protected String extension = null;
	protected String filename = "";
	
	protected MyStreamResponse(InputStream is, String filename) {
		this.filename = filename;
		inputStream = is;
	}

	protected MyStreamResponse(InputStream is) {
		inputStream = is;
	}

	@Override
	public String getContentType() {
		return contentType;
	}

	@Override
	public InputStream getStream() throws IOException {
		return inputStream;
	}

	@Override
	public void prepareResponse(Response response) {
		response.setHeader("Expires", "0");
		response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
		response.setHeader("Pragma", "public");
	}
}
