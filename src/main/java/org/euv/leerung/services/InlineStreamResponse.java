/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.services;

import java.io.InputStream;

import org.apache.tapestry5.services.Response;

/**
 * The response will displayed inline. The browser tries to render
 * this content directly. 
 * @author Marcus Schneider
 *
 */
public class InlineStreamResponse extends MyStreamResponse {
	public InlineStreamResponse(InputStream is, String filename) {
		super(is, filename);
	}
	
	public InlineStreamResponse(InputStream is) {
		super(is);
	}
	
	@Override
	public void prepareResponse(Response response) {
		super.prepareResponse(response);
		response.addHeader("Content-Disposition", "inline; filename=" + filename + ((extension == null) ? "" : ("." + extension)));
  }
}
