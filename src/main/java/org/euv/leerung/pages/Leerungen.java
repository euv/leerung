/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.pages;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.EventConstants;
import org.apache.tapestry5.Link;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Log;
import org.apache.tapestry5.annotations.OnEvent;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.BaseURLSource;
import org.apache.tapestry5.services.PageRenderLinkSource;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.SelectModelFactory;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.euv.leerung.rest.api.HausnummerApi;
import org.euv.leerung.rest.api.LeerungApi;
import org.euv.leerung.rest.model.Hausnummer;
import org.euv.leerung.rest.model.LeerungenFeiertagverschiebung;
import org.euv.leerung.services.IcsGeneratorService;
import org.euv.leerung.services.PdfGeneratorService;
import org.euv.leerung.services.PdfStreamResponse;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Page for displaying the Leerungen for a hausId 
 * @author Marcus Schneider
 */
@Component
@Import(library={"context:js/clip.js"})
public class Leerungen {
	@Inject
	private Logger logger;
	
	@Inject
	private AjaxResponseRenderer ajaxResponseRenderer;
	
	@Inject
	private Request request;
	
	@Inject
	private SelectModelFactory selectModelFactory;
	
	@Inject
	PdfGeneratorService pdfGeneratorService;
	
	@Inject
	IcsGeneratorService icsGeneratorService;
	
	@Inject
	BaseURLSource baseURLSource;

	
	
	@Property
	@Persist
	private Long hausId;
	
	@Property
	@Persist
	private Long strassenId;
	
	@Property
	@Persist
	private Long year;
	
	@Property
	@Persist
	private Long month;
	
	@Property
	private SelectModel modelYears;
	
	@Property
	@Persist
	private String strassenname;
	
	@Property
	@Persist
	private String hausnummer;
	
	@Property
	@Persist
	private LeerungenFeiertagverschiebung leerungenFeiertagverschiebung;
	
	@InjectComponent
	private Zone zoneLeerung;
	
	
	
	@InjectPage
	private Index index;
	
	@Inject
	@Autowired
	private HausnummerApi hausnummerApi;
	
	@Inject
	@Autowired
	private LeerungApi leerungApi;
	
	@Inject
	PageRenderLinkSource pageRenderLinkSource;
	
	

	/**
	 * Must be called from previous page. Sets the parameters for this page.
	 * @param hausId The haId from Trash
	 * @param strassenId The stId from Trash
	 */
	public void set(Long hausId, Long strassenId) {
		this.hausId = hausId;
		this.strassenId = strassenId;
	}
	
	/**
	 * Will be called before rendering. Intialize the class.
	 */
	void setupRender() {
		logger.debug("hausId: {}", hausId);

		initDateFields();
		initLeerungen();
	}

	private void initDateFields() {
		LocalDate ld = new LocalDate();
		if(modelYears==null) {
			List<Long> listYears = new ArrayList<>();
			listYears.add(Long.valueOf(ld.getYear()));
			if(ld.getMonthOfYear()>11) {
				listYears.add(Long.valueOf((long)ld.getYear() + 1));
			}
			modelYears = selectModelFactory.create(listYears);
		}
		
		if(year==null) {
			year = Long.valueOf(ld.getYear());
		}
		if(month==null) {
			month = Long.valueOf(ld.getMonthOfYear());
		}
	}

	private boolean initLeerungen() {
		try {
			if(hausId!=null) {
				leerungenFeiertagverschiebung = leerungApi.getLeerungFeiertagsverschiebungByHaIdUsingGET(hausId, true);
			
				Hausnummer hausnummerById = hausnummerApi.getByIdUsingGET(hausId, true);
				this.strassenname = hausnummerById.getStrasse();
				this.hausnummer = hausnummerById.getNummer();
				return true;
			} else {
				return false;
			}
		}
		catch(Exception e) {
			return false;
		}
	}
	
	@Log
	Object onActivate() {
		logger.debug("onActivate member hausId {}", this.hausId);
		
		if(this.hausId==null) {
			return index;
		}

		return null;
	}
	
	Object onGoMainpage() {
		return Index.class;
	}

	/**
	 * Event handler if year in select component changed.
	 * @param year The new year
	 */
	@OnEvent(component="selectYear", value=EventConstants.VALUE_CHANGED)
	void onValueChangedYear(Long year) {
		logger.debug("year: {}", year);
		if(request.isXHR()) {
			ajaxResponseRenderer.addRender(zoneLeerung);
		}
	}

	/**
	 * Event handler if month in select component changed.
	 * @param month The new month
	 */
	@OnEvent(component="selectMonth", value=EventConstants.VALUE_CHANGED)
	void onValueChangedMonth(Long month) {
		logger.debug("month: {}", month);
		if(request.isXHR()) {
			ajaxResponseRenderer.addRender(zoneLeerung);
		}
	}
	
	/**
	 * Event handler for request a PDF file
	 * @return The stream for the PDF output
	 */
	@Log
	public StreamResponse onRequestPdf() {
		String filename = "EUV-Abfallkalender-"+ year.toString() +"-"+ strassenname +" "+ hausnummer;
		InputStream is;
		try {
			is = pdfGeneratorService.generatePdf(year.intValue(), hausId);
			return new PdfStreamResponse(is, filename);
		} catch (IOException e) {
			logger.error("IO Exception catched in PDF", e);
		}
		return null;
	}
	
	@Log
	public Link onRequestIcs() {
		return pageRenderLinkSource.createPageRenderLink(Ics.class).addParameterValue("haId", hausId);
	}
	
	
	
	public Boolean getHasValidValues() {
		return (year!=null && month!=null && leerungenFeiertagverschiebung!=null);
	}
	
	public String getBaseUrl() {
		if(request.getServerPort()!=80 && request.getServerPort()!=443) {
			return "https://"+request.getServerName()+":"+request.getServerPort();
		} else {
			return "https://"+request.getServerName();
		}
	}
}
