/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.pages;

import org.apache.tapestry5.EventContext;
import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Log;
import org.apache.tapestry5.annotations.Persist;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.HttpError;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.euv.leerung.model.HausnummerIdSelectModel;
import org.euv.leerung.model.StrassenIdSelectModel;
import org.euv.leerung.rest.api.HausnummerApi;
import org.euv.leerung.rest.api.StrasseApi;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.validation.constraints.NotNull;

/**
 * Start page of application leerung.
 * @author Marcus Schneider
 */
@Component
public class Index {
	@Inject
	private Logger logger;

	@Inject
	private AjaxResponseRenderer ajaxResponseRenderer;
	
	@Inject
	private Request request;

	@Inject
	@Autowired
	private StrasseApi strasseApi;
	
	@Inject
	@Autowired
	private HausnummerApi hausnummerApi;

	
	@InjectComponent("zoneStrasse")
	private Zone zoneStrasse;

	@InjectComponent("zoneHausnummer")
	private Zone zoneHausnummer;

	@InjectComponent("zoneSubmit")
	private Zone zoneSubmit;


	@Property
	@Persist
	@NotNull
	private Long strassenId;
	
	@Property
	@Persist
	private Long hausId;
	
	@Property
	private SelectModel strassenModel;
	
	@Property
	private SelectModel hausnummernModel;
	
	@InjectPage
	private Leerungen leerungenPage;
	
	// Handle call with an unwanted context
	Object onActivate(EventContext eventContext) {
		return eventContext.getCount() > 0 ? new HttpError(404, "Resource not found") : null;
	}
	
	void onPrepare() {
		try {
			strassenModel = new StrassenIdSelectModel(strasseApi.listUsingGET1());
			if(strassenId == null) {
				hausnummernModel = new HausnummerIdSelectModel(null);
			} else {
				try {
					hausnummernModel = new HausnummerIdSelectModel(hausnummerApi.listUsingGET(strassenId, false));
				}
				catch(Exception e) {
					hausnummernModel = new HausnummerIdSelectModel(null);
				}
			}
		}
		catch(Exception e) {
			strassenModel = new StrassenIdSelectModel(null);
			hausnummernModel = new HausnummerIdSelectModel(null);
		}
	}

	void onValueChangedFromSelectStrasse(Long stId) {
		logger.debug("onValueChanged: {}", stId);
		logger.debug("strassenId: {}", strassenId);
		hausId = null;
		if(stId==null) {
			hausnummernModel = new HausnummerIdSelectModel(null);
		} else {
			try {
				hausnummernModel = new HausnummerIdSelectModel(hausnummerApi.listUsingGET(stId, false));
			}
			catch(Exception e) {
				hausnummernModel = new HausnummerIdSelectModel(null);
			}
		}
		if(request.isXHR()) {
			ajaxResponseRenderer.addRender(zoneHausnummer);
			ajaxResponseRenderer.addRender(zoneSubmit);
		}
	}
	
	public boolean getDisableSubmit() {
		return hausId==null;
	}
	
	void onValueChangedFromSelectHausnummern(Long hausId) {
		logger.debug("onValueChanged Hausnummer: {}", hausId);
		if(request.isXHR()) {
			ajaxResponseRenderer.addRender(zoneSubmit);
		}
	}

	@Log
	Object onSuccess() {
		logger.debug("onSuccess");
		logger.debug("strasseId: {}", strassenId);
		logger.debug("HausId: {}", hausId);
		
		leerungenPage.set(hausId, strassenId);
		return leerungenPage;
	}
	
	@Log
	void onFailure() {
		logger.debug("onFailure");
	}
}
