/*
 * Leerungsabfrage
 *  
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 * Marcus Schneider <marcus.schneider@euv-stadtbetrieb.de>
 */
package org.euv.leerung.pages;

import java.io.IOException;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.annotations.ActivationRequestParameter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.euv.leerung.services.IcsGeneratorService;
import org.euv.leerung.services.IcsStreamResponse;
import org.slf4j.Logger;

/**
 * This page will generate a ICAL ics file
 * @author Marcus Schneier
 */
public class Ics {
	@Inject
	Logger logger;
	
	@Inject
	IcsGeneratorService icsGeneratorService;

	@ActivationRequestParameter
	private Long haId; 
	
	public StreamResponse onActivate() throws IOException {
		return new IcsStreamResponse(icsGeneratorService.generateIcs(haId), "leerung");
	}
}
