var _paq = window._paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function() {
	var u="https://piwik.euv-stadtbetrieb.de/";
	_paq.push(['setTrackerUrl', u+'matomo.php']);
	_paq.push(['setSiteId', '3']);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
})();

function trackPdf() {
	_paq.push(['trackGoal', 1]);
}

function trackICal() {
	_paq.push(['trackGoal', 2]);
}

function trackUrlCopy() {
	_paq.push(['trackGoal', 3]);
}
