FROM openjdk:8-jdk-stretch AS build

ARG http_proxy=http://proxy.euvnt01.euv-stadtbetrieb.de:3128
ARG https_proxy=http://proxy.euvnt01.euv-stadtbetrieb.de:3128
ARG TZ=Europe/Berlin

RUN apt-get update && apt-get install -y unzip

COPY . /build/
COPY configs/gradle.properties /build/gradle.properties

WORKDIR /build
RUN rm -rf build && \
  ./gradlew build

RUN unzip -d /ROOT /build/build/libs/leerung.war



FROM tomcat:9-jre8-slim

ARG warfile

LABEL maintainer="marcus.schneider@euv-stadtbetrieb.de"
LABEL version=1.0
LABEL description="Leerungsabfrage WebApp des EUVs"
LABEL vendor="EUV Stadtbetrieb Castrop-Rauxel -AöR-"

ENV http_proxy http://proxy.euvnt01.euv-stadtbetrieb.de:3128
ENV https_proxy http://proxy.euvnt01.euv-stadtbetrieb.de:3128

ENV PROXY_SCHEMA http
ENV PROXY_HOST euvproxy01.euvnt01.euv-stadtbetrieb.de
ENV PROXY_PORT 3128

ENV API_URL https://api.euv-stadtbetrieb.de/leerung/v1
ENV API_TAGE_URL https://api.euv-stadtbetrieb.de/planungstage

ENV PRODUCTION_MODE true

WORKDIR /usr/local/tomcat

ADD tools/dockerize-alpine-linux-amd64-v0.6.1.tar.gz /usr/local/bin/

RUN rm -r /usr/local/tomcat/webapps/ROOT/ && \
  mkdir -p /euv/config/
#COPY --from=build /build/build/libs/leerung.war /usr/local/tomcat/webapps/ROOT.war
COPY --from=build /ROOT /usr/local/tomcat/webapps/ROOT
COPY configs/server.tmpl /euv/config/tomcat/server.xml
COPY configs/settings.tmpl /euv/config/WEB-INF/settings.properties
COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh

VOLUME /usr/local/tomcat/conf/

EXPOSE 8080

#CMD ["/usr/local/bin/dockerize", "-template", "/euv/config/server.tmpl:/usr/local/tomcat/conf/server.xml", "-template", "/euv/config/settings.tmpl:/usr/local/tomcat/webapps/ROOT/WEB-INF/settings.properties", "catalina.sh", "run"]
CMD ["/entrypoint.sh"]
