#!/bin/sh

echo Prepare WEB-INF
for f in /euv/config/WEB-INF/* ; do
    b=`basename $f`
    echo Copy file $b
    dockerize -template ${f}:/usr/local/tomcat/webapps/ROOT/WEB-INF/${b}
done

echo Prepare server.xml
for f in /euv/config/tomcat/* ; do
    b=`basename $f`
    echo Copy file $b
    dockerize -template ${f}:/usr/local/tomcat/conf/${b}
done

exec /usr/local/bin/dockerize catalina.sh run
